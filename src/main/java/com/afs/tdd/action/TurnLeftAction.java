package com.afs.tdd.action;

import com.afs.tdd.MarsRover;

public class TurnLeftAction implements Action{
    @Override
    public void execute(MarsRover marsRover) {
        marsRover.turnLeft();
    }
}
