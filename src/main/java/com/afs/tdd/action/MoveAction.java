package com.afs.tdd.action;

import com.afs.tdd.MarsRover;
import com.afs.tdd.action.Action;

public class MoveAction implements Action {
    @Override
    public void execute(MarsRover marsRover) {
        marsRover.move();
    }
}
