package com.afs.tdd.action;

import com.afs.tdd.MarsRover;

public interface Action {
    void execute(MarsRover marsRover);
}
