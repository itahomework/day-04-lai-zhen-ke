package com.afs.tdd.action;

import com.afs.tdd.MarsRover;

public class TurnRightAction implements Action{
    @Override
    public void execute(MarsRover marsRover) {
        marsRover.turnRight();
    }
}
