package com.afs.tdd;

import java.util.Objects;

public class Location {
    private int CoordinateX;
    private int CoordinateY;
    private Direction direction;

    public Location(int CoordinateX, int CoordinateY, Direction direction) {
        this.CoordinateX = CoordinateX;
        this.CoordinateY = CoordinateY;
        this.direction = direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return CoordinateX == location.CoordinateX && CoordinateY == location.CoordinateY && direction == location.direction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(CoordinateX, CoordinateY, direction);
    }

    public int getCoordinateX() {
        return CoordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        this.CoordinateX = coordinateX;
    }

    public int getCoordinateY() {
        return CoordinateY;
    }

    public void setCoordinateY(int coordinateY) {
        this.CoordinateY = coordinateY;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
