package com.afs.tdd;

import com.afs.tdd.action.Action;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
    private Location location;
    private List<Action> actionList;

    public MarsRover(Location location) {
        this.location = location;
        this.actionList = new ArrayList<>();
    }

    public void addAction(Action action){
        this.actionList.add(action);
    }
    public Location executeAction(){
        for (Action action : actionList){
            action.execute(this);
        }
        return this.location;
    }

    public Location executeCommand(Command command) {
        if (command == Command.Move){
            move();
        }
        if (command == Command.Left){
            turnLeft();
        }
        if (command == Command.Right){
            turnRight();
        }
        return this.location;
    }

    public void move(){
        switch (this.location.getDirection()){
            case North:
                this.location.setCoordinateY(this.location.getCoordinateY() + 1);
                break;
            case West:
                this.location.setCoordinateX(this.location.getCoordinateX() - 1);
                break;
            case South:
                this.location.setCoordinateY(this.location.getCoordinateY() - 1);
                break;
            case East:
                this.location.setCoordinateX(this.location.getCoordinateX() + 1);
                break;
        }
    }

    public void turnLeft(){
        switch (this.location.getDirection()){
            case North:
                this.location.setDirection(Direction.West);
                break;
            case West:
                this.location.setDirection(Direction.South);
                break;
            case South:
                this.location.setDirection(Direction.East);
                break;
            case East:
                this.location.setDirection(Direction.North);
                break;
        }
    }

    public void turnRight(){
        switch (this.location.getDirection()){
            case North:
                this.location.setDirection(Direction.East);
                break;
            case West:
                this.location.setDirection(Direction.North);
                break;
            case South:
                this.location.setDirection(Direction.West);
                break;
            case East:
                this.location.setDirection(Direction.South);
                break;
        }
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
