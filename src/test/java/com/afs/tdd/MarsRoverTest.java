package com.afs.tdd;

import com.afs.tdd.action.MoveAction;
import com.afs.tdd.action.TurnLeftAction;
import com.afs.tdd.action.TurnRightAction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MarsRoverTest {

    @Test
    void should_change_to_0_1_N_when_executeCommand_given_0_0_N_and_command_move() {
        // given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new MoveAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(0, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(1, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterAction.getDirection());
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_0_0_N_and_command_left() {
        // given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new TurnLeftAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(0, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(0, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterAction.getDirection());
    }

    @Test
    void should_change_to_0_0_E_when_executeCommand_given_0_0_N_and_command_right() {
        // given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new TurnRightAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(0, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(0, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterAction.getDirection());
    }

    @Test
    void should_change_to_Minus1_0_W_when_executeCommand_given_0_0_W_and_command_move() {
        // given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new MoveAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(-1, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(0, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterAction.getDirection());
    }

    @Test
    void should_change_to_0_0_S_when_executeCommand_given_0_0_W_and_command_left() {
        // given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new TurnLeftAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(0, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(0, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterAction.getDirection());
    }

    @Test
    void should_change_to_0_0_N_when_executeCommand_given_0_0_W_and_command_right() {
        // given
        Location locationInitial = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new TurnRightAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(0, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(0, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterAction.getDirection());
    }

    @Test
    void should_change_to_0_Minus1_S_when_executeCommand_given_0_0_S_and_command_move() {
        // given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new MoveAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(0, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(-1, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterAction.getDirection());
    }

    @Test
    void should_change_to_0_0_E_when_executeCommand_given_0_0_S_and_command_left() {
        // given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new TurnLeftAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(0, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(0, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterAction.getDirection());
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_0_0_S_and_command_right() {
        // given
        Location locationInitial = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new TurnRightAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(0, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(0, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.West, locationAfterAction.getDirection());
    }

    @Test
    void should_change_to_1_0_E_when_executeCommand_given_0_0_E_and_command_move() {
        // given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new MoveAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(1, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(0, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.East, locationAfterAction.getDirection());
    }

    @Test
    void should_change_to_0_0_N_when_executeCommand_given_0_0_E_and_command_left() {
        // given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new TurnLeftAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(0, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(0, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterAction.getDirection());
    }

    @Test
    void should_change_to_0_0_S_when_executeCommand_given_0_0_E_and_command_right() {
        // given
        Location locationInitial = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new TurnRightAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(0, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(0, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.South, locationAfterAction.getDirection());
    }

    @Test
    void should_change_location_to_Minus1_1_North_when_executeCommand_given_location_0_0_North_and_batch_commands(){
        // given
        Location locationInitial = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(locationInitial);
        marsRover.addAction(new MoveAction());
        marsRover.addAction(new TurnLeftAction());
        marsRover.addAction(new MoveAction());
        marsRover.addAction(new TurnRightAction());
        // when
        Location locationAfterAction = marsRover.executeAction();
        // then
        Assertions.assertEquals(-1, locationAfterAction.getCoordinateX());
        Assertions.assertEquals(1, locationAfterAction.getCoordinateY());
        Assertions.assertEquals(Direction.North, locationAfterAction.getDirection());
    }
}
